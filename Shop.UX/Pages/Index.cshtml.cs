﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Shop.Application.CreateProduct;
using Shop.Application.GetProduct;
using Shop.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.UX.Pages
{
    public class IndexModel : PageModel
    {

        private ApplicationDbContext _ctx;

        public IndexModel(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        [BindProperty]
        public Application.CreateProduct.CreateProduct.ProductViewModel Product { get; set; }

        public IEnumerable<Application.GetProduct.ProductViewModel> Products { get; set; }

        public void OnGet()
        {
            Products = new GetProduct(_ctx).Do();
        }

        public async Task <IActionResult> OnPost()
        {
            await new CreateProduct(_ctx).Do(Product);

            return RedirectToAction("Index");
        }

        //private readonly ILogger<IndexModel> _logger;

        //public IndexModel(ILogger<IndexModel> logger)
        //{
        //    _logger = logger;
        //}


    }
}
